<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Diocesis */

$this->title = 'Create Diocesis';
$this->params['breadcrumbs'][] = ['label' => 'Dioceses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diocesis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
