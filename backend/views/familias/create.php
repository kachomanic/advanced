<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Familias */

$this->title = 'Create Familias';
$this->params['breadcrumbs'][] = ['label' => 'Familias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="familias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
