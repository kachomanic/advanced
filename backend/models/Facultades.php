<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "facultades".
 *
 * @property integer $codFacultad
 * @property string $nombre
 *
 * @property Matriculas[] $matriculas
 * @property PlanEstudio[] $planEstudios
 * @property Asignaturas[] $codAsignaturas
 */
class Facultades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'facultades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codFacultad' => 'Cod Facultad',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matriculas::className(), ['codFacultad' => 'codFacultad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanEstudios()
    {
        return $this->hasMany(PlanEstudio::className(), ['codFacultad' => 'codFacultad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodAsignaturas()
    {
        return $this->hasMany(Asignaturas::className(), ['codAsignatura' => 'codAsignatura'])->viaTable('plan_estudio', ['codFacultad' => 'codFacultad']);
    }
}
