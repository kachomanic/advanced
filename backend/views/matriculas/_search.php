<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MatriculasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matriculas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codMatricula') ?>

    <?= $form->field($model, 'semestre') ?>

    <?= $form->field($model, 'añoAcademico') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'codFacultad') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
