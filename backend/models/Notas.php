<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notas".
 *
 * @property integer $codGrupo
 * @property integer $codMatricula
 * @property integer $codEstudiante
 * @property boolean $presencial_especial
 * @property boolean $tutoria
 * @property string $nota
 *
 * @property Estudiantes $codEstudiante0
 * @property Grupos $codGrupo0
 * @property Matriculas $codMatricula0
 */
class Notas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codGrupo', 'codMatricula', 'codEstudiante', 'nota'], 'required'],
            [['codGrupo', 'codMatricula', 'codEstudiante'], 'integer'],
            [['presencial_especial', 'tutoria'], 'boolean'],
            [['nota'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codGrupo' => 'Cod Grupo',
            'codMatricula' => 'Cod Matricula',
            'codEstudiante' => 'Cod Estudiante',
            'presencial_especial' => 'Presencial Especial',
            'tutoria' => 'Tutoria',
            'nota' => 'Nota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstudiante0()
    {
        return $this->hasOne(Estudiantes::className(), ['codEstudiante' => 'codEstudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodGrupo0()
    {
        return $this->hasOne(Grupos::className(), ['codGrupo' => 'codGrupo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodMatricula0()
    {
        return $this->hasOne(Matriculas::className(), ['codMatricula' => 'codMatricula']);
    }
}
