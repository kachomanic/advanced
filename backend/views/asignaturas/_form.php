<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Asignaturas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asignaturas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numAsignatura')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'especificaciones')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'totalHoras')->textInput(['maxlength' => 3]) ?>

    <?= $form->field($model, 'credito')->textInput(['maxlength' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
