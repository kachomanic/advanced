<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Diocesis */

$this->title = 'Update Diocesis: ' . ' ' . $model->codDiocesis;
$this->params['breadcrumbs'][] = ['label' => 'Dioceses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codDiocesis, 'url' => ['view', 'id' => $model->codDiocesis]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="diocesis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
