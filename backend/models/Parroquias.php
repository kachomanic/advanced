<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "parroquias".
 *
 * @property integer $codParroquia
 * @property string $nombre
 * @property string $municipio
 * @property integer $codDiocesis
 *
 * @property Estudiantes[] $estudiantes
 * @property Diocesis $codDiocesis0
 */
class Parroquias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parroquias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'municipio', 'codDiocesis'], 'required'],
            [['codDiocesis'], 'integer'],
            [['nombre'], 'string', 'max' => 80],
            [['municipio'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codParroquia' => 'Cod Parroquia',
            'nombre' => 'Nombre',
            'municipio' => 'Municipio',
            'codDiocesis' => 'Cod Diocesis',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiantes()
    {
        return $this->hasMany(Estudiantes::className(), ['codParroquia' => 'codParroquia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodDiocesis0()
    {
        return $this->hasOne(Diocesis::className(), ['codDiocesis' => 'codDiocesis']);
    }
}
