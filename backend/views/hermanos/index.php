<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HermanosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hermanos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hermanos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Hermanos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codHermano',
            'codEstudiante',
            'cantHermanos',
            'cantHermanas',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
