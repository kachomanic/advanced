<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Hermanos */

$this->title = 'Update Hermanos: ' . ' ' . $model->codHermano;
$this->params['breadcrumbs'][] = ['label' => 'Hermanos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codHermano, 'url' => ['view', 'id' => $model->codHermano]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hermanos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
