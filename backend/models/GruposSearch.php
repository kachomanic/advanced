<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Grupos;

/**
 * GruposSearch represents the model behind the search form about `backend\models\Grupos`.
 */
class GruposSearch extends Grupos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codGrupo', 'codAsignatura', 'codDocente'], 'integer'],
            [['numGrupo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Grupos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codGrupo' => $this->codGrupo,
            'codAsignatura' => $this->codAsignatura,
            'codDocente' => $this->codDocente,
        ]);

        $query->andFilterWhere(['like', 'numGrupo', $this->numGrupo]);

        return $dataProvider;
    }
}
