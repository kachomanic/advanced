<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Especialidades */

$this->title = 'Update Especialidades: ' . ' ' . $model->codEspecialidad;
$this->params['breadcrumbs'][] = ['label' => 'Especialidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codEspecialidad, 'url' => ['view', 'id' => $model->codEspecialidad]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="especialidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
